package com.example.infimonkapp;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private JSONArray response, data;
    private String TAG = MapsActivity.class.getSimpleName();
    private String url = "http://142.93.206.95:3000/geofence";
    SupportMapFragment mapFragment;
    private Button shaortestDistanceButton, serverUpdate;
    private static float smallest = -1f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        initView();
    }

    public void initView() {
        shaortestDistanceButton = findViewById(R.id.shotest_distance);
        shaortestDistanceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcualteShortestDistance();
            }
        });
        serverUpdate = findViewById(R.id.server_update);
        serverUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    updateDataToServer();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        loadData();
    }

    private void loadData() {
        RequestQueue queue = Volley.newRequestQueue(this);

        response = new JSONArray();
        JsonRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d(TAG, "JSON Response" + response.get("data"));
                    data = response.getJSONArray("data");
                    JSONObject obj = data.getJSONObject(0);
                    Log.d(TAG, "id: " + obj.get("geofenceId") + " latitude: " + obj.get("latitude") + " longitude: " + obj.get("longitude"));
                    drawInMap();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Volley Error: " + error.getMessage());
            }
        });
        queue.add(jsonRequest);

    }

    private void calcualteShortestDistance() {
        float d1[] = new float[10];
        float d2[] = new float[10];
        float d3[] = new float[10];
        try {
            Location.distanceBetween(Double.parseDouble(data.getJSONObject(0).get("latitude").toString()), Double.parseDouble(data.getJSONObject(0).get("latitude").toString()),
                    Double.parseDouble(data.getJSONObject(1).get("latitude").toString()), Double.parseDouble(data.getJSONObject(1).get("latitude").toString()), d1);
            Location.distanceBetween(Double.parseDouble(data.getJSONObject(1).get("latitude").toString()), Double.parseDouble(data.getJSONObject(1).get("latitude").toString()),
                    Double.parseDouble(data.getJSONObject(2).get("latitude").toString()), Double.parseDouble(data.getJSONObject(2).get("latitude").toString()), d2);
            Location.distanceBetween(Double.parseDouble(data.getJSONObject(2).get("latitude").toString()), Double.parseDouble(data.getJSONObject(2).get("latitude").toString()),
                    Double.parseDouble(data.getJSONObject(0).get("latitude").toString()), Double.parseDouble(data.getJSONObject(0).get("latitude").toString()), d3);
            smallest = d1[0];
            if (smallest > d2[0]) {
                smallest = d2[0];
            }
            if (smallest > d3[0]) {
                smallest = d3[0];
            }
            Log.d(TAG, "Smallest: " + smallest);
            smallest = smallest / 1000;
            Toast.makeText(MapsActivity.this, "Smallest distance between the given points is " + smallest + " KM", Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            Toast.makeText(MapsActivity.this, "Unexpected error occurred!..Please check your internet connection.", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

    }

    private void updateDataToServer() throws JSONException {
        if (smallest != -1) {
            JSONObject jsonObject = new JSONObject();
            Random number = new Random();
            jsonObject.put("userId", number.nextInt(100));
            jsonObject.put("distance", smallest);

            RequestQueue queue = Volley.newRequestQueue(this);

            response = new JSONArray();
            JsonRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.get("response").toString().equals("success") && Integer.parseInt(response.get("status").toString()) == 200) {

                            Toast.makeText(MapsActivity.this, "Data inserted successfully", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(MapsActivity.this, "Failed to insert data", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        Toast.makeText(MapsActivity.this, "Failed to insert data", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "Volley Error: " + error.getMessage());
                    Toast.makeText(MapsActivity.this, "Failed to insert data", Toast.LENGTH_LONG).show();
                }
            });
            queue.add(jsonRequest);
        } else {
            Toast.makeText(MapsActivity.this, "No data to update", Toast.LENGTH_LONG).show();
        }
    }

    private void drawInMap() throws JSONException {
        for (int i = 0; i < data.length(); i++) {
            try {
                JSONObject obj = data.getJSONObject(i);
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(Double.valueOf(obj.get("latitude").toString()), Double.valueOf(obj.get("longitude").toString())))
                        .title("geofenceId " + obj.get("geofenceId")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        moveCamera(new LatLng(Double.parseDouble(data.getJSONObject(1).get("latitude").toString()), Double.parseDouble(data.getJSONObject(1).get("longitude").toString())), 12);
    }

    private void moveCamera(LatLng latLng, float zoom) {
        Log.d(TAG, "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapFragment.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapFragment.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapFragment.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapFragment.onDestroy();
    }
}

